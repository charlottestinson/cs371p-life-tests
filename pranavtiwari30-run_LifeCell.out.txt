*** Life<Cell> 24x7 ***

Generation = 0, Population = 28.
-------
-0-----
-0-----
-0-0---
0----0-
------0
------0
-------
-----0-
--0----
-----0-
----0--
---00--
-------
-------
0--0---
--0---0
---0---
--0-00-
-------
--0-0--
---0---
--0----
---0--0

Generation = 110, Population = 11.
.......
.......
......-
.......
......-
...-...
.......
.....-.
.......
...-...
....-..
...-...
.......
**.....
*.*....
.*.....
...-...
.......
.....*.
....*.*
....*.*
.....*.
...-...
......-

*** Life<Cell> 8x25 ***

Generation = 0, Population = 9.
---------------------0--0
----------0------------0-
0----0-------------------
-------------------------
0------------------------
-----------------------0-
-------------------------
----------------------0--

Generation = 188, Population = 11.
.........................
.........................
.........................
**.......................
*.*......................
.*...........**..........
............*..*.........
.............**..........

*** Life<Cell> 7x9 ***

Generation = 0, Population = 28.
--0--00--
----0---0
00--00---
-00-00-0-
--0-000--
-0000-00-
00---0--0

Generation = 126, Population = 0.
.........
.........
..-.....-
.........
.........
.........
.........

Generation = 252, Population = 0.
.........
.........
..-.....-
.........
.........
.........
.........

*** Life<Cell> 6x17 ***

Generation = 0, Population = 17.
-----0---00---0--
0-------0-----0--
-0--0-----0---0--
0----------------
----0------------
--------00-0--0--

*** Life<Cell> 15x12 ***

Generation = 0, Population = 10.
------------
------------
---------0--
-----------0
--0---------
0-----------
------------
------------
-----------0
--0---------
------------
-------0----
--0---------
--------0---
-------0----

Generation = 53, Population = 33.
....**......
..****......
..*...*.....
.*..**......
..****......
..**....***.
........*.*.
.......*..*.
.......*.*..
.......***..
............
............
..........1.
.........*-.
.........**.

Generation = 106, Population = 8.
............
............
............
............
............
............
............
............
............
............
.....**.**..
.....**.**..
............
............
............

Generation = 159, Population = 8.
............
............
............
............
............
............
............
............
............
............
.....**.**..
.....**.**..
............
............
............

Generation = 212, Population = 8.
............
............
............
............
............
............
............
............
............
............
.....**.**..
.....**.**..
............
............
............

Generation = 265, Population = 8.
............
............
............
............
............
............
............
............
............
............
.....**.**..
.....**.**..
............
............
............

*** Life<Cell> 33x28 ***

Generation = 0, Population = 27.
--------------------------0-
-----------------------0----
----0-----------------------
----------------------------
----------------------------
----------------------0-----
---------------------0------
----------------------------
----------------------0-----
-----0----------------------
----0-----------------------
---------------0------------
------------------0---------
-----0----------------------
----------------------------
----0-----------------------
---0---------0--------------
-----------0----------------
0---------------------------
--------------0-------------
-----------------0----------
----------0------------0----
----------------------------
----------------------------
------0-------0-------------
0---------------------------
----------------------------
----------------------------
--0-0-----------------------
---------------0------------
--------------------0-------
----------------------------
----------------------------

Generation = 34, Population = 238.
.........-**..**..*.........
.*.1-*...*.*.1**..*.........
***..*.****.......***.......
***..-.-.*...........*......
**.-....-***.........*......
......*..*.*......*..**....-
...-.****...............-...
....*.....****..............
..*...**-.**.***.*.**......*
.1*...*..1...**.*....*....**
...****.*.1..1*.***..*......
.........**..........*......
......-.*....**...**........
..............*...........*-
....*.**........*.*...-*.*..
**.***..*......**.........**
*.....**.-.......**....***-*
*....*.......**...........*.
**.*.........**..1****......
**............*.-*..*.....*.
.-*.......***.....*......*..
..***...*****.....**...**.-.
.......*-*.**.........*.*...
......*.***........*...*....
.....**..**..........1......
.....*..****.......-........
.....*.............-........
.*................*..**..*.-
.....*.**.**.....*.***.-.***
.****...***.*......*...**-..
0***......*...**.....*..****
.*1*....-..***...*..-......-
............*.1*1....**.*-*.

Generation = 68, Population = 145.
.*........**................
*..***....**................
..**...***..*...............
.......**.****..............
.......*....*...............
****.*.*....*..............-
.**.*......*.........**.**..
.....*...**..........**..**.
..***..*............***..*.*
...*.**....................*
..**.**..................**.
...**.**........*...........
....*.*........*.....*......
.....*.........*.**.**......
...........**..*.**...*.....
...........**..*..**........
...................**.......
.........*.....**..*........
........*.*....**...........
.......**.*...*.........*...
.-......**....*.*......**...
..............*.*......*..-.
...............**...........
............................
............................
.**.........................
*.*...................**..**
*......................*..*.
**......................***.
.........................*..
...**.......................
...**........**..**........-
.............**..**......-..

Generation = 102, Population = 99.
....****....................
..**...**...................
.**..*.*.*..........*.......
*.*......**........*.*......
**.**..*...........*.*......
.*...*...*..........*......-
......*..*..................
....*.....*.................
....*.....*..............**.
....*......***..........*..*
........*****.****.......**.
............*......*........
........***..*****.**.......
........***....**.*.........
............................
............................
...............***..........
...............**...........
..................*.........
............................
............................
..........................-.
............................
............................
............................
............................
............................
....................*.......
.....................**.....
...................*.**.....
...**............***.**.....
...**...........**.**.......
............................

Generation = 136, Population = 37.
............................
............................
....................*.......
...................*.*......
...................*.*......
....................*......-
...............**...........
...............**...........
.........................**.
........................*..*
......**.................**.
.....*..*......**...........
......**.......*............
.............*.*............
.............**.............
............................
............................
............................
............................
............................
............................
..........................-.
............................
............................
............................
............................
............................
............................
............................
..........................**
...**.....................**
...**.......................
............................

Generation = 170, Population = 37.
............................
............................
....................*.......
...................*.*......
...................*.*......
....................*......-
...............**...........
...............**...........
.........................**.
........................*..*
......**.................**.
.....*..*......**...........
......**.......*............
.............*.*............
.............**.............
............................
............................
............................
............................
............................
............................
..........................-.
............................
............................
............................
............................
............................
............................
............................
..........................**
...**.....................**
...**.......................
............................

Generation = 204, Population = 37.
............................
............................
....................*.......
...................*.*......
...................*.*......
....................*......-
...............**...........
...............**...........
.........................**.
........................*..*
......**.................**.
.....*..*......**...........
......**.......*............
.............*.*............
.............**.............
............................
............................
............................
............................
............................
............................
..........................-.
............................
............................
............................
............................
............................
............................
............................
..........................**
...**.....................**
...**.......................
............................

Generation = 238, Population = 37.
............................
............................
....................*.......
...................*.*......
...................*.*......
....................*......-
...............**...........
...............**...........
.........................**.
........................*..*
......**.................**.
.....*..*......**...........
......**.......*............
.............*.*............
.............**.............
............................
............................
............................
............................
............................
............................
..........................-.
............................
............................
............................
............................
............................
............................
............................
..........................**
...**.....................**
...**.......................
............................

Generation = 272, Population = 37.
............................
............................
....................*.......
...................*.*......
...................*.*......
....................*......-
...............**...........
...............**...........
.........................**.
........................*..*
......**.................**.
.....*..*......**...........
......**.......*............
.............*.*............
.............**.............
............................
............................
............................
............................
............................
............................
..........................-.
............................
............................
............................
............................
............................
............................
............................
..........................**
...**.....................**
...**.......................
............................

*** Life<Cell> 5x3 ***

Generation = 0, Population = 13.
000
000
0-0
-00
000

Generation = 160, Population = 0.
..-
...
...
...
...

*** Life<Cell> 47x31 ***

Generation = 0, Population = 21.
-------------------------------
--0----------------------------
-------------------------------
-------------------------------
-------------------------------
-------0--------0--------------
-------------------------------
-------------------------------
--------------0----------------
-------------------------------
-------------------------------
-------------------------------
-------------------------------
-------------------------------
----------------------------0--
-------------------------------
-------------------------------
-------------------------------
-------------------------------
-------------------------------
-------------------------------
-------------------------------
-------------------------------
-------------------------------
-------------------------------
-------------------------------
----0--------------------------
----0-----------0--------------
----------0--------------------
-------------------------------
-------------------------------
----------------0--------------
-------------------0-----------
-----------------0-----------0-
-------------------------------
--0----------------------------
-----------------------0-------
-----0-------------------------
-------------------------------
-------------------------------
-0-----------------------------
-------------------------------
-------------------------------
-----------0-------------------
------------0----0-------------
-------------------------------
--------------0----------------

*** Life<Cell> 31x47 ***

Generation = 0, Population = 9.
-----------------------------------------------
-----------------------------------------------
-------------------------------0---------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------0-----------
-----------------------------------------------
-------------------------0---------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------0-----
---------------------------------------------0-
-----------------------------------------------
-----------------------------------------------
--0--------------------------------------------
-----------------------------------------------
----------------------------0------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
------------------------------------------0----
-----------------------------------------------
------------------------------------------0----
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------

Generation = 71, Population = 242.
.*...*.**......................................
*.*..*.**.*.*.*..---....***....................
.**...*.***.***.........*.*.......-............
.***......*.***-........***....................
...............................................
.........**....**....-.........................
.........***...***.............................
.-...*..*..*...**.*.....*......................
.........*........*....*.*..............**.....
.........*.***.***.......*..............**.....
.....*....*****.*....*.*.......................
.......*.....**.....**.......................**
....*...**....*.*............*.............****
...............*............*.**...........*...
.........*.....*............*..*..........*..**
....**.**....................***...........****
**..*.*.........**....***........-.....**..**..
*............****.*...*.*.............**.......
...........**.*.*..............................
**.........**.*...*.......*.............*..**..
**.........**.*..........**............****.*..
*-....**.....*....*........*...........******..
**....*...**......*.....***....................
*..**..**............*.****....................
**..**........*.......*........................
.**..***...*..*................................
...........*..*........*.......................
-..*...*.*..*.*...-...*****....................
....*...*............*...****.*...............-
..-.*..............-..********............-....
.......................*.......................

Generation = 142, Population = 101.
...............................................
...........**..................................
.....*.....**.....**...........................
.....*............**...........................
.....*.........................................
.....................-.........................
...............................................
.-.........................***.................
........................................**.....
........................................**.....
.............................*.................
............................*.*................
............................*.*................
......*........................................
.....*.*.......................................
.....*..*.................**...................
......**..................**...................
..........................*....................
...................***...**.............**.....
..................*...*.****.*.........*..*....
.........*.......*....*.*.....**........**.....
........***................*****.............**
................*..**...*....*..*............**
.................*....**.**....................
........***......**.*..........................
.........*.......**.*..........................
**.............***.*...........................
-*.............**.-............................
..............................................-
...................-......................-....
...............................................

*** Life<Cell> 32x3 ***

Generation = 0, Population = 18.
---
---
---
-0-
---
00-
---
--0
-00
---
0--
0--
---
---
0-0
-0-
---
-0-
-0-
---
-0-
---
0--
---
---
---
--0
--0
---
---
---
--0

Generation = 124, Population = 3.
.-.
...
...
...
...
...
...
.-.
..-
...
...
...
-..
...
...
...
.*-
.**
...
...
..-
...
...
...
.-.
...
...
...
...
...
...
...

*** Life<Cell> 4x20 ***

Generation = 0, Population = 12.
----------------0-0-
0-----0-------------
0---------00---0-0--
---------0----0----0

*** Life<Cell> 9x24 ***

Generation = 0, Population = 10.
--0---------0-----------
------------------------
------------------------
--0-----------0---------
0-----0---------------0-
------------------0-----
------0-0---------------
------------------------
------------------------

Generation = 196, Population = 19.
....**............**...-
...*..*..........*..*...
....**............**....
.......................1
.......................-
.*......................
*.*......-..............
*.*.....................
.*...................-..

*** Life<Cell> 42x34 ***

Generation = 0, Population = 17.
----------------------------------
----------------------------------
----------------------------------
----------------------0-----------
----------------------------------
----------------------------------
----------------------------------
----------------------------------
----------0-----------------------
----------------------------------
-------------------0--------------
----------------------------------
-----------------------0----------
----------------------------------
----------------------------------
----------------------------------
----------------------------------
----------------------------------
----------------------------------
----------------------------------
----------------------------------
--------------0-------------------
----------------------0----0------
----------------------------------
----------------------------------
-------------------0--------------
---------------------------------0
--------------------0-------------
----------------------------------
----------------------------------
-----------------------0----------
----------------------------------
--------0-------------------------
----------------------------------
----------------0------------0----
----------------------------------
----------------------------------
----------------------------------
----------------------------------
-------0--0-----------------------
-------------------------0--------
----------------------------------

Generation = 60, Population = 241.
...................**............-
...............*.....*........*...
..........**...-**.*....*....**..1
..........**....*........*..**..*.
.................*......*..***....
..................*...**....*.....
...................*..........*...
--.....................*..*..**...
.........................******...
........**......................1.
.......*.*......................**
......***......................***
................................*.
..................*****...........
.....-.................*..........
.*................*...............
*..*.*.................*..........
-.***.*..............*.........*..
..**.*......-.***............*..*.
...*.-.......*..**................
...*........*...*.*.....**...***..
.......**....**.*.*......*...*....
.....*...**......*.*......***.....
....*..*......*...**.......*......
....*......*.*.**...*.............
..*...***.*.......................
.........*.......*................
..*.............***...............
....*....*...**..**...............
...**.*.**........*..............-
....**........*...**...........-..
............**....**..............
................***...............
-...........***................*..
..............*.*......*......**..
......***......**.....*.*....***..
-....*....*....**.....*.*...*.....
....*.....***.*.*****.***..**.....
....***....*.**.****..**..**......
.-...*.*....*...*....**....*.*....
.......**..*..*.******......**....
.......**.**...*..**........1.....

*** Life<Cell> 44x36 ***

Generation = 0, Population = 10.
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
-----------------------------------0
------------------------------------
------------------------------------
----------0-------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------0-----------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
-----------------------------------0
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
-----0----0-------------------------
------------------------------------
0--0-----0-----------------------0--
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------
------------------------------------

Generation = 180, Population = 130.
--............**...............--.-.
.............*..*.................--
.......**.....*.*................-..
......*..*.....*..............-...--
.......**...........................
....................................
...................................-
........................**..........
.......................*..*.........
........................*.*.........
.........................*..........
.*..................................
..**................................
..***...............*...............
..*..*..*...........*...............
...*.**.*...........*...............
....................................
....**..........***...***......*....
...............................**...
..............................*.*...
....................................
....................................
....................................
...............*....................
..............*.*...................
.............*.*......*.............
............**.......***............
**............*......*..*.........**
**....*.....*.*..*******...........*
......*......*.**...**...........*.*
......*........*.....*...........**.
................*...*...............
..***...***.......**................
.......................*........**..
......*...............*...*.....**..
......*.............**.....*........
......*.............**.*............
....................**...*..........
......................*.*...........
....................................
................**..................
...............*..*.................
................**..................
.......................-............

*** Life<Cell> 27x24 ***

Generation = 0, Population = 30.
--0------0--------------
------0-----------------
---0--------0---0------0
------------------------
------------------------
------------------------
------------------------
-------0----------------
-------------0-0---00---
------------------------
---0--------------------
-0------------------0---
--------------0---------
-0-----------------0----
-------------------0----
----------------------0-
-----00---0---0---------
------------------------
------------------------
---------------0----0---
------------------------
------------------------
------------------------
------------------------
---------------------0--
-----0------------------
------00----------------

Generation = 31, Population = 119.
-...1**...*....*........
....**..*.*......*..**..
....**.*.**...**..*.....
..***.*.........**......
-.**.-..................
....................*...
-.-..-...-.......-.**...
.-..*1.........**.*-....
...-*..........**...*...
...*.*.......-***.**....
-.--.*..**....*....*....
..**.*-.....******.*....
*....*...........*......
....*............*......
..................**....
**............-**...*...
-...**........*..**.....
....*.........*******...
-..............*1..**...
..-.............*.***...
...-...-...-.-.*.*.***-.
............-.....1...*.
.................***-..*
............-*..0......*
..............*.*****..*
...-............**..1.-.
........................

Generation = 62, Population = 26.
-...-...................
..........*.............
........*..*............
........*.*.............
-........*..............
......*.....*...........
-.....*.....*...........
......*.....*......-....
........................
........***.............
-.......................
........................
........................
........................
........................
................*.......
-...**.........*.*......
....**........*..*......
-..............**.......
..-.....................
...-...-...-.-........-.
............-...........
....................-...
............-...........
........................
...-..................-.
........................

Generation = 93, Population = 17.
-...-...................
........................
........................
........................
-.......................
........................
-....***................
...................-....
.........*..............
.........*..............
-........*..............
........................
........................
........................
........................
................*.......
-...**.........*.*......
....**........*..*......
-..............**.......
..-.....................
...-...-...-.-........-.
............-...........
....................-...
............-...........
........................
...-..................-.
........................

Generation = 124, Population = 17.
-...-...................
........................
........................
........................
-.......................
......*.................
-.....*.................
......*............-....
........................
........***.............
-.......................
........................
........................
........................
........................
................*.......
-...**.........*.*......
....**........*..*......
-..............**.......
..-.....................
...-...-...-.-........-.
............-...........
....................-...
............-...........
........................
...-..................-.
........................

*** Life<Cell> 2x3 ***

Generation = 0, Population = 6.
000
000

Generation = 50, Population = 0.
-.-
-.-

Generation = 100, Population = 0.
-.-
-.-

Generation = 150, Population = 0.
-.-
-.-

Generation = 200, Population = 0.
-.-
-.-

Generation = 250, Population = 0.
-.-
-.-

*** Life<Cell> 18x24 ***

Generation = 0, Population = 26.
------------------0-----
-------------------0---0
0----------0--------0---
-------00---------------
------------------------
-0----------------------
0--0----------0-------0-
------------------------
-0-----0----------------
-0--------------0-------
------------------------
-----------0----0-------
-----0--0---------------
------------------------
------------------------
---------0-------0------
------0-----------------
--0----------------0----

Generation = 47, Population = 84.
..........**.**.........
......*..*......*...**..
......*....*....*...**..
.......*...**....*......
..........*...*...*.....
.........**..*....**....
**...........*.*...*....
*.*..................*..
.*......**..*....*.***..
.......**..**.*...*..*..
........**....****.*....
.........*..*......***..
..........*.*.*.**.*....
.........*..**.*.**.....
.*........*.*...........
*.*.............**......
*..*....................
.**.....................

*** Life<Cell> 31x5 ***

Generation = 0, Population = 6.
-----
---0-
-----
-----
---0-
-----
-----
-----
-----
-----
-----
-----
-----
----0
-----
-----
-----
-----
-----
-----
-----
-----
-----
-----
0---0
-----
-----
-----
0----
-----
-----

Generation = 93, Population = 0.
-....
.....
.....
.....
....-
.....
.....
.....
.....
.....
.....
-....
.....
.....
.....
.....
.....
.....
.....
.....
.....
.....
.....
.....
.....
.....
.....
.....
.....
.....
.....

*** Life<Cell> 10x26 ***

Generation = 0, Population = 20.
--------------------------
------------00------------
0--------0-----0----------
-------0--0---------------
--------00------0---------
--------------------------
-----0-0-------0--------0-
------0-----------0-------
0---------------0---------
0-0-----------------------

Generation = 38, Population = 36.
.............-......-...-.
..........................
..........................
....*...............*.....
...**......**......*.*....
.***.*.....*.*..........*.
.*...*.......*.-....**.**.
.*.........***.....***..*.
....................***...
..***..............-......

Generation = 76, Population = 16.
.............-......-...-.
..........................
..........................
.....................*....
**..................*.*...
**..................*.*...
.....................*....
.................**.......
.................*.*......
..................**......

*** Life<Cell> 11x7 ***

Generation = 0, Population = 12.
-------
-------
--0----
00-----
-------
--0----
---0---
---0---
00--0-0
----0--
-0-----

Generation = 176, Population = 6.
.......
.......
.......
.......
.......
..**...
.*..*..
..**...
.......
.......
.......

*** Life<Cell> 17x17 ***

Generation = 0, Population = 14.
-----------------
0----------------
----0---0--------
------------0----
-----------------
-----------------
-----------0-----
---0-------------
---0---0---0--0--
----------0------
-----------------
---0-------------
-----------------
-----0-----------
-----------------
------------0----
-----------------

Generation = 118, Population = 17.
.................
.................
.............-1..
.......*.........
......*.*........
......*.*........
.......*.........
.................
.................
.................
.................
.................
....*............
...*.*........*..
..*..*........*..
...**.........*..
-................

Generation = 236, Population = 17.
.................
.................
.............-1..
.......*.........
......*.*........
......*.*........
.......*.........
.................
.................
.................
.................
.................
....*............
...*.*........*..
..*..*........*..
...**.........*..
-................

Generation = 354, Population = 17.
.................
.................
.............-1..
.......*.........
......*.*........
......*.*........
.......*.........
.................
.................
.................
.................
.................
....*............
...*.*........*..
..*..*........*..
...**.........*..
-................
